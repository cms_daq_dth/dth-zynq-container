#!/bin/bash
container_name="dth-zynq-xdaq-container"

if [ -z "$1" ]; then
    echo "Usage: $0 <command_to_run>"
    exit 1
fi

sudo podman exec -it $container_name /bin/bash -c "$1"